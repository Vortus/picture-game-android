package andzelika.korteles;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import andzelika.korteles.Objects.Card;

/*
    Čia vyksta žaidimas.
*/

public class GameActivity extends AppCompatActivity {

    public static Card[] cards; // Kortelių lentelė
    private Thread gameThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        initializeCards(); // Inicializuojam korteles
        generateTable(); // Generuojam lentelę

        // Resetinam globalius kintamuosius
        Variables.timePassed = 3; // Uždėti kiek sekundžių galės žiūrėti sekundžių
        Variables.clickedCount = 0;
        Variables.filled = 0;
        Variables.gameRunning = true;

        // Pagrindinis ciklas
            gameThread = new Thread(new Runnable() {
            @Override
            public void run() {

                boolean gameStarted = false;
                long secondsTimer = System.currentTimeMillis(); // Sekundžių laikmatis

                while(Variables.gameRunning) { // Kol žaidimas vyksta

                    // Sekundžių laikmatis
                    if(System.currentTimeMillis() - secondsTimer > 1000) { // Kas sekundę tikrinam

                        // Tikrinam ar prasidėjo žaidimas
                        if(Variables.timePassed < 0 && !gameStarted){
                            gameStarted = true;
                            flipTable();
                        }

                        if(!gameStarted){ // Jei neprasidėjes atiminejam iš laiko kol baigsis skaičius
                            changeTimeCount(Variables.timePassed + " !", true); // Keičiam laiką
                            Variables.timePassed--;
                            secondsTimer = System.currentTimeMillis();
                        } else { // Pridedam laiką
                            Variables.timePassed++;
                            changeTimeCount(Variables.timePassed + " s", true); // Keičiam laiką
                            secondsTimer = System.currentTimeMillis();
                        }

                    }

                }

                runOnUiThread(new Runnable() { // Veikiam ant pagrindinės užduoties
                    @Override
                    public void run() {
                        if(Variables.filled >= 14) // Jei Baigėm žaidimą
                            Toast.makeText(GameActivity.this, "Užtrukai " + Variables.timePassed + " s", Toast.LENGTH_LONG).show(); // Išmetam žinutę
                        finish();
                    }
                });

                finish(); // Užbaigiam žaidimą
            }
        });

        gameThread.setDaemon(true); // Kad išjunktu žaidimą išsijungus ir langui
        gameThread.start(); // Pradedam žaidimo veiksmą
    }

    @Override
    public void onBackPressed() { // Kai paspaudžia atgal, išjungiam žaidimą
        Variables.gameRunning = false;
        finish();
    }

    private void changeTimeCount(final String text, final boolean enableShow){ // Pakeičia laiko tekstą
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView timeView = (TextView) findViewById(R.id.timeCount);
                if (enableShow) timeView.setVisibility(View.VISIBLE);
                timeView.setText(text);
            }
        });
    }

    private void flipTable(){ // Apverčia visą lentelę
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for(int i = 0; i < 16; i++) {
                    cards[i].flip();
                    cards[i].setClickable(true);
                }
            }
        });
    }

    private void generateTable(){ // Sugeneruoja pozicijas
        List<Integer> list = new ArrayList<Integer>();
        Random random = new Random();

        while(list.size() < 16){ // Generacija
            int number = random.nextInt(8);
            while(Collections.frequency(list, number) > 1)
                number = random.nextInt(8);
            list.add(number);
        }

        for(int i = 0; i < 16; i++) // Priskiriam pozicijas
            cards[i].setImageID(list.get(i));
    }

    private void initializeCards(){
        cards = new Card[16]; // Inicializuojam kortelių lentelę
        cards[0] = new Card(this, 0, (ImageView) findViewById(R.id.imageView));
        cards[1] = new Card(this, 1, (ImageView) findViewById(R.id.imageView2));
        cards[2] = new Card(this, 2, (ImageView) findViewById(R.id.imageView3));
        cards[3] = new Card(this, 3, (ImageView) findViewById(R.id.imageView4));
        cards[4] = new Card(this, 4, (ImageView) findViewById(R.id.imageView5));
        cards[5] = new Card(this, 5, (ImageView) findViewById(R.id.imageView6));
        cards[6] = new Card(this, 6, (ImageView) findViewById(R.id.imageView7));
        cards[7] = new Card(this, 7, (ImageView) findViewById(R.id.imageView8));
        cards[8] = new Card(this, 8, (ImageView) findViewById(R.id.imageView9));
        cards[9] = new Card(this, 9, (ImageView) findViewById(R.id.imageView10));
        cards[10] = new Card(this, 10, (ImageView) findViewById(R.id.imageView11));
        cards[11] = new Card(this, 11, (ImageView) findViewById(R.id.imageView12));
        cards[12] = new Card(this, 12, (ImageView) findViewById(R.id.imageView13));
        cards[13] = new Card(this, 13, (ImageView) findViewById(R.id.imageView14));
        cards[14] = new Card(this, 14, (ImageView) findViewById(R.id.imageView15));
        cards[15] = new Card(this, 15, (ImageView) findViewById(R.id.imageView16));
    }

}

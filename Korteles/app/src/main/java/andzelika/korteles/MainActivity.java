package andzelika.korteles;

/*
    Pagrindinis meniu.
*/

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Variables.loadImageIDs(); // Užkraunam paveikslėlius

        Button button = (Button) findViewById(R.id.buttonStart);
        button.setOnClickListener(new View.OnClickListener() { // Ar paspaudė starto knopkę
            @Override
            public void onClick(View v) { // Kai paspaudė
                startActivity(new Intent(MainActivity.this, GameActivity.class)); // Iškviečiam žaidimo langą
            }
        });

    }

}

package andzelika.korteles.Objects;

/*
    Kortelės apibrėžimas.
*/

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import andzelika.korteles.GameActivity;
import andzelika.korteles.ImageConverter;
import andzelika.korteles.R;
import andzelika.korteles.Variables;

public class Card {

    private ImageView imageView; // Kortelės paveikslėlis
    private Context context;

    private int imageID, ID; // Kortelės identifikatorius
    private boolean facing;
    private boolean clickable;

    public Card(Context context, final int ID, ImageView imageView){
        this.imageView = imageView;
        this.context = context;
        this.ID = ID;
        facing = false;
        clickable = true;
        setImage(context, R.drawable.p0, 40);// Pilkas paveikslėlis su 40px kampų raundinimu

        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(clickable) { // Jei eina paspausti ant mygtuko

                    /*
                    Variables.clickedList yra sarašas preitų dviejų atverstų langelių.
                    Jame saugomi jų ID 0-15
                     */

                    if(Variables.clickedList.size() == 2){ // Jei prieš tai jau du langeliai atidaryti

                        if(GameActivity.cards[Variables.clickedList.get(0)].ID != // Ar nevienodi ID
                                GameActivity.cards[Variables.clickedList.get(1)].ID &&
                                GameActivity.cards[Variables.clickedList.get(0)].imageID == // Ar vienodi Paveikslėliai
                                        GameActivity.cards[Variables.clickedList.get(1)].imageID){ // Jei atradom porą

                            GameActivity.cards[Variables.clickedList.get(0)].setClickable(false); // Padarom kad neitu paspaust
                            GameActivity.cards[Variables.clickedList.get(1)].setClickable(false);

                            Variables.filled += 2; // Užpildyti + 2 lang

                        } else { // Užverčiam praeitus jei nepataikė teisingų
                            GameActivity.cards[Variables.clickedList.get(0)].flip(false); // Atverčiam
                            GameActivity.cards[Variables.clickedList.get(1)].flip(false);
                        }


                        Variables.clickedList.clear(); // Išvalom sarašą

                        flip(); // Atverčiam dabartinį kuris buvo paspaustas
                        Variables.clickedList.add(ID); // pridedam jį į saraša

                    } else {
                        if(Variables.filled >= 14) // Ar baigėsi žaidimas
                            Variables.gameRunning = false;

                        flip(); // Apverčiam
                        Variables.clickedList.add(ID); // Pridedam paspaustą į sarašą
                    }

                }

            }
        });

    }

    public void flip(boolean on){ // Apvertimo funkcija su parametru kaip apversti
        if(!on){
            facing = false;
            setImage(context, R.drawable.p0, 40); // Pilkas paveikslėlis su 40px kampų raundinimu
        } else {
            facing = true;
            setImage(context, Variables.imageIDs[getImageID()], 40); // Jam priskirtas paveikslėlis su 40px kampų raundinimu
        }
    }

    public void flip(){ // Tiesiog apvertimo funkcija
        if(facing){
            facing = false;
            setImage(context, R.drawable.p0, 40);// Pilkas paveikslėlis su 40px kampų raundinimu
        } else {
            facing = true;
            setImage(context, Variables.imageIDs[getImageID()], 40);// Jam priskirtas paveikslėlis su 40px kampų raundinimu
        }
    }

    // ----------------- Getteriai seteriai -------------------

    public void setImage(Context context, int id, int pixels){
        Bitmap img = BitmapFactory.decodeResource(context.getResources(), id);
        imageView.setImageBitmap(ImageConverter.getRoundedCornerBitmap(img, pixels)); // Uždedam paveikslėlį suroindintą
    }

    public int getImageID() {
        return imageID;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
        setImage(context, Variables.imageIDs[imageID], 40);
        facing = true;
        clickable = false;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }

}

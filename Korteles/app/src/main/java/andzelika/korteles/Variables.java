package andzelika.korteles;

/*
    Statiniai kintamieji
*/

import java.util.ArrayList;
import java.util.List;

public class Variables {

    public static boolean gameRunning = false;
    public static int timePassed = 0;
    public static int clickedCount = 0;
    public static int imageIDs[] = new int[8];
    public static int filled = 0;
    public static List<Integer> clickedList = new ArrayList<Integer>();

    public static void loadImageIDs(){
        imageIDs[0] = R.drawable.p1;
        imageIDs[1] = R.drawable.p2;
        imageIDs[2] = R.drawable.p3;
        imageIDs[3] = R.drawable.p4;
        imageIDs[4] = R.drawable.p5;
        imageIDs[5] = R.drawable.p6;
        imageIDs[6] = R.drawable.p7;
        imageIDs[7] = R.drawable.p8;
    }

}
